-- Writes a TGF object to a file.

local format = require('string').format

return function(fp, tgf)
  local return_string = nil
  local out

  if (tgf == nil) then
    tgf = fp
    return_string = ''
    out = function(s)
      return_string = return_string..s
    end
  elseif (type(fp) == 'function') then
    out = fp
  else
    out = function(s)
      assert(fp:write(s))
    end
  end

  out("TGF 1\n")
  out(tostring(tgf.width).."\t"..tostring(tgf.height).."\n")

  for blkid,blkdat in pairs(tgf.misc_blocks) do
    out(blkid.."\n")
    out(' '..blkdat.."\n")
  end

  out("PLTE\n")

  for _,color in ipairs(tgf.palette) do
    out(' ')
    out(format('%0.2X', color.r))
    out(format('%0.2X', color.g))
    out(format('%0.2X', color.b))
    out(format('%0.2X', color.a))
    out("\n")
  end

  out("IDAT\n")

  do
    local fmtstr
    do
      local hexlen = #tostring(format('%X', #tgf.palette - 1))
      fmtstr = ' %0.'..tostring(hexlen)..'X'
    end

    for _,row in ipairs(tgf.data) do
      for _,v in ipairs(row) do
        out(string.format(fmtstr, v - 1))
      end

      out("\n")
    end
  end

  if (return_string) then
    return return_string
  end
end
