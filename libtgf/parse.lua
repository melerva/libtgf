-- Parses a TGF file, returns a TGF object.

-- Create a copy of the default greyscale palette.
local default_palette = function()
  local plte = {}

  for i = 0, 255, 1 do
    plte[i + 1] = {
      r = i,
      g = i,
      b = i,
      a = 255,
    }
  end

  return plte
end

local floor = require('math').floor

return function(stream, v1, v2)
  local getln

  if (type(stream) == 'function') then
    getln = function()
      v2 = stream(v1, v2)

      if (v2) then
        if (v2:sub(-1) == "\n") then
          return v2:sub(1, #v2 - 1)
        end
      end

      return v2
    end
  else
    getln = function()
      return stream:read('l')
    end
  end

  local result = {}

  do
    local magic = getln()
    assert(magic == 'TGF 1', 'Invalid signature')
  end

  do -- get dimensions
    local dims = getln()
    local w, h = dims:match("^(%d+)[ \t](%d+)$")

    if not (w and h) then
      error('Invalid format for image dimensions')
    end

    result.height = tonumber(h)
    result.width = tonumber(w)
  end

  local blocks = {}

  do
    local current_block = nil

    while (true) do
      local line = getln()

      if (line == nil) then
        break
      end

      if (#line > 0) then
        if (line:match("^[ \t]")) then -- block content
          assert(current_block, 'Data outside of block')
          blocks[current_block] = blocks[current_block]..line:sub(2)..' '
        else -- new block
          current_block = line

          if (blocks[current_block] == nil) then
            blocks[current_block] = ''
          end
        end
      end
    end
  end

  assert(blocks['IDAT'], 'Missing image data.')

  if (blocks['PLTE']) then -- process palette
    result.palette = {}

    local raw_entry = ''

    for i = 1, #blocks['PLTE'], 1 do
      local c = blocks['PLTE']:sub(i, i)

      if ((c ~= ' ') and (c ~= "\t")) then
        raw_entry = raw_entry..c

        if (#raw_entry >= 8) then
          -- Since Lua starts at 1 instead of 0, the palette is all off by 1.
          result.palette[#result.palette + 1] = {
            r = tonumber(raw_entry:sub(1, 2), 16),
            g = tonumber(raw_entry:sub(3, 4), 16),
            b = tonumber(raw_entry:sub(5, 6), 16),
            a = tonumber(raw_entry:sub(7, 8), 16),
          }

          raw_entry = ''
        end
      end
    end

    blocks['PLTE'] = nil
  else -- gr[ae]yscale
    result.palette = default_palette()
  end

  do -- process data
    blocks['IDAT'] = blocks['IDAT']..' '
    result.data = {}

    for y = 1, result.height, 1 do
      result.data[y] = {}
    end

    local value = ''

    local offset = 0

    for i = 1, #blocks['IDAT'], 1 do
      local c = blocks['IDAT']:sub(i, i)

      if ((c ~= ' ') and (c ~= "\t")) then
        value = value..c
      elseif (#value > 0) then
        local y = floor((offset) / result.width) + 1
        local x = ((offset) % result.width) + 1

        result.data[y][x] = tonumber(value, 16) + 1
        -- The +1 is to match the quirk with the palette above.

        value = ''
        offset = offset + 1
      end
    end

    blocks['IDAT'] = nil
  end

  result.misc_blocks = blocks

  return result
end
