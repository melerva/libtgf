#! /usr/bin/lua5.3

local open = require('io').open
local tgf = require('libtgf')

local options = {
  strip_palette = false, -- -s
  minify = false,        -- -x
}

local input
local output

-- process args
do
  local skip_opts = false

  local i = 1
  while (i <= #arg) do
    if not (skip_opts) then
      if (arg[i] == '--') then
        skip_opts = true
        goto continue
      end

      if (arg[i] == '-s') then
        options.strip_palette = true
        goto continue
      end

      if (arg[i] == '-x') then
        options.minify = true
        goto continue
      end

      if (arg[i] == '--help') then
        local write = require('io').write

        write("USAGE:\n")
        write("\t"..arg[0].." [flags] [input] [output]\n")
        write(" Both <input> and <output> default to stdin/stdout.\n")
        write("FLAGS:\n")
        write("\t-s\tRemove unused palette entries\n")
        write("\t-x\tMake the output as small as possible (not pretty)\n")
        return
      end
    end

    if not (input) then
      input = arg[i]
      goto continue
    end

    if not (output) then
      output = arg[i]
      goto continue
    end

    error('Unknown argument #'..tostring(i)..': "'..arg[i]..'"')

    ::continue::
    arg[i] = nil -- don't need it anymore
    i = i + 1
  end
end

arg = nil -- don't need it anymore

local filedata

-- read file
if (input == '-') then
  filedata = tgf.parse(require('io').stdin)
else
  local fp = assert(open(input, 'r'))
  filedata = tgf.parse(fp)
  fp:close()
  input = nil
end

-- do operations

if (options.strip_palette) then -- {{{
  local new_palette = {}

  local used_indices = {}

  local function next_used(idx)
    for i = idx, #filedata.palette, 1 do
      if (used_indices[i]) then
        return i
      end
    end
    return nil
  end

  for _,r in pairs(filedata.data) do
    for c,v in pairs(r) do
      used_indices[v] = true
    end
  end

  for i = 1, #filedata.palette, 1 do
    if (used_indices[i]) then
      new_palette[i] = filedata.palette[i]
    else
      local new_idx = next_used(i)

      if (new_idx) then
        new_palette[i] = filedata.palette[new_idx]

        for y = 1, filedata.height, 1 do
          for x = 1, filedata.width, 1 do
            if (filedata.data[y][x] == new_idx) then
              filedata.data[y][x] = i
            end
          end
        end

        used_indices[new_idx] = nil
        used_indices[i] = true
      end
    end
  end

  filedata.palette = new_palette
end -- }}}

local minify = function(fp, data) -- {{{
  local format = require('string').format

  assert(fp:write("TGF 1\n"))
  assert(fp:write(tostring(data.width)..' '..tostring(data.height).."\n"))
  for blkid,blkdat in pairs(data.misc_blocks) do
    assert(fp:write(blkid.."\n"))
    assert(fp:write(' '..blkdat.."\n"))
  end
  assert(fp:write("PLTE\n "))
  for _,color in ipairs(data.palette) do
    assert(fp:write(format('%0.2X', color.r)))
    assert(fp:write(format('%0.2X', color.g)))
    assert(fp:write(format('%0.2X', color.b)))
    assert(fp:write(format('%0.2X', color.a)))
  end
  assert(fp:write("\n"))
  assert(fp:write("IDAT\n"))
  for _,row in ipairs(data.data) do
    for _,v in ipairs(row) do
      assert(fp:write(string.format(' %X', v - 1)))
    end
  end
end -- }}}

-- write result
if (output) then
  local fp = assert(open(output, 'wb'))
  if (options.minify) then
    minify(fp, filedata)
  else
    tgf.write(fp, filedata)
  end
  fp:close()
else
  if (options.minify) then
    minify(require('io').stdout, filedata)
  else
    tgf.write(require('io').stdout, filedata)
  end
end
